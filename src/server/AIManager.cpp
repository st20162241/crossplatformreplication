#include "AIManager.h"

std::unique_ptr<AIManager> AIManager::sInstance;

AIManager::AIManager()
{

}

AIManager::~AIManager() 
{
    
}

bool AIManager::StaticInit()
{
    sInstance.reset(new AIManager());
    return true;
}

void AIManager::Update()
{
    for (auto controller : mControllers) {
        controller.second->Update();
    }
}

void AIManager::RegisterPlayer(PlayerServer* player)
{
    player->SetPlayerId(mNextPlayerID);
    PlayerController* controller = new PlayerController();
    controller->RegisterPlayer(player);
    mControllers[mNextPlayerID] = controller;
    mNextPlayerID--;
}

PlayerController* AIManager::GetPlayerController(int playerID)
{
    PlayerController* controller = mControllers[playerID];
    if (controller) {
        return controller;
    }
    return nullptr;
}
