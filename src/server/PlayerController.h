#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include <memory>
#include <array>

#include "PlayerServer.h"

class PlayerController {
public:
    PlayerController();
    ~PlayerController();

    void RegisterPlayer(PlayerServer* player);

    void Update();

    Vector3 GetTargetLocation() {return mNodes[mNodeIndex];}
    float GetTargetRotation() {return mTargetRotation;}

private:
    PlayerServer* mPlayer;

    int mNodeIndex;
    std::array<Vector3, 10> mNodes;
    float mTargetRotation;
};

#endif