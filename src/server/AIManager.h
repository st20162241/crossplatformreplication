#ifndef AIMANAGER_H
#define AIMANAGER_H

#include <memory>
#include <map>

#include "PlayerServer.h"
#include "PlayerController.h"

class AIManager {
public:
    virtual ~AIManager();
    static std::unique_ptr<AIManager> sInstance;

    static bool StaticInit();

    void Update();

    void RegisterPlayer(PlayerServer* player);

    PlayerController* GetPlayerController(int playerID);

private:
    AIManager();

    int mNextPlayerID;

    std::map<int, PlayerController*> mControllers;
};

#endif