#include "PlayerController.h"
#include "OutputMemoryBitStream.h"

PlayerController::PlayerController()
{

}

PlayerController::~PlayerController()
{

}

void PlayerController::RegisterPlayer(PlayerServer* player)
{
    mPlayer = player;
    mPlayer->SetPlayerControlType(ESCT_AI);
}

void PlayerController::Update()
{
    // uint32_t dirtyState;

    mTargetRotation = mPlayer->GetRotation();
    mTargetRotation += 0.1f;
}