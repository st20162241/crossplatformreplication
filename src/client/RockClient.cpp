#include "RockClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

RockClient::RockClient()
{
    mSpriteComponent.reset(new SpriteComponent(this));
    mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("rock"));
}

void RockClient::Update()
{

}

void RockClient::Read(InputMemoryBitStream& inInputStream)
{
    bool stateBit;

    uint32_t readState = 0;

    inInputStream.Read(stateBit);
    if (stateBit)
    {
        Vector3 replicatedLocation;
        inInputStream.Read(replicatedLocation.mX);
        inInputStream.Read(replicatedLocation.mY);
        SetLocation(replicatedLocation);

        readState |= ECRS_Pose;
    }

    inInputStream.Read(stateBit);
    if (stateBit)
    {
        Vector3 color;
        inInputStream.Read(color);
        SetColor(color);
        readState |= ECRS_Color;
    }


}