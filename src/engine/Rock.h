#ifndef ROCK_H
#define ROCK_H

#include "GameObject.h"
#include "World.h"

class Rock : public GameObject
{
public:
    CLASS_IDENTIFICATION( 'ROCK', GameObject )

    /*
    Copied from player - removed ID and health as these are not needed
    */
    enum ERockReplicationState
	{
		ECRS_Pose = 1 << 0,
		ECRS_Color = 1 << 1,

		ECRS_AllState = ECRS_Pose | ECRS_Color
	};

    static GameObject *StaticCreate() { return new Rock(); }
    static GameObjectPtr StaticCreatePtr() { return GameObjectPtr(new Rock()); }
    virtual uint32_t GetAllStateMask() const override { return ECRS_AllState; }
    virtual void Update() override;

    uint32_t Write( OutputMemoryBitStream &inOutputStream, uint32_t inDirtyState ) const override;

protected:
    Rock();
};

typedef shared_ptr<Rock> RockPtr;

#endif