#include "Rock.h"
#include "OutputMemoryBitStream.h"

Rock::Rock() : GameObject() {
    SetCollisionRadius(0.5f);
}

void Rock::Update() {

}

uint32_t Rock::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
    uint32_t writtenState = 0;

    if (inDirtyState & ECRS_Pose)
    {
        inOutputStream.Write((bool)true);

        Vector3 location = GetLocation();
        inOutputStream.Write(location.mX);
        inOutputStream.Write(location.mY);

        writtenState |= ECRS_Pose;
    }
    else
    {
        inOutputStream.Write((bool)false);
    }

    if (inDirtyState & ECRS_Color)
    {
        inOutputStream.Write((bool)true);
        inOutputStream.Write(GetColor());

        writtenState |= ECRS_Color;
    }
    else
    {
        inOutputStream.Write((bool)false);
    }

    return writtenState;
}